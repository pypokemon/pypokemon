import random, time
class Pokemon(object):

    def __init__(self, name, HP, Damage):
        self.name = name     #Sets the name of the Pokemon
        self.HP = HP         #The Hit Points or health of this pokemon
        self.Damage = Damage #The amount of Damage this pokemon does every     attack


    def Battle(self, Opponent):
        attackDamage = self.Damage

        if(self.HP > 0): #While your pokemon is alive it will coninute the Battle
            print("%s did %d Damage to %s"%(self.name, attackDamage, Opponent.name)) #Text-based combat descriptors
            time.sleep(.1)
            print("%s has %d HP left"%(Opponent.name,Opponent.HP)) #Text-based descriptor for the opponent's health
            time.sleep(.1)

            Opponent.HP -= attackDamage #The damage you inflict upon the opponent is subtracted here
            return Opponent.Battle(self)  #Now the Opponent pokemon attacks
        else:
            print("\n%s wins! (%d HP left)" %(Opponent.name, Opponent.HP)) #declares the winner of the Battle
            return Opponent, self  #return a tuple (Winner, Loser)


print("Leader: Prepare to be defeated by my Squirtle! Your Bulbasaur is no match!\n\n")
time.sleep(2)


Squirtle = Pokemon('Squirtle', 100, random.randint(10,30))
Bulbasaur = Pokemon('Bulbasaur', 100, random.randint(10,30))
Winner, Loser = Bulbasaur.Battle(Squirtle)
loop2 = True
loop = True
while loop != False:

    retry = input("\n\nLeader: Good fight! Want me to heal your Pokemon so we can battle again? (y/n)\n\n")
    if retry == "y":
        print("Leader: Prepare to be defeated by my Squirtle! Your Bulbasaur is no match!\n\n")
        time.sleep(2)
        Squirtle = Pokemon('Squirtle', 100, random.randint(10,30))
        Bulbasaur = Pokemon('Bulbasaur', 100, random.randint(10,30))
        Winner, Loser = Bulbasaur.Battle(Squirtle)



    elif retry == "n":
        input("\n\nLeader: All right! I'll heal your Pokemon. Come back anytime for a battle! (enter to exit)")
        exit()

    else:
        print(" * Invalid response, try again. *")
        loop = True

input()